// 1
let listItems = fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'GET',
	headers: {
		'Content-type': 'application/json'
	}
})
.then(response => response.json())
.then(json => json.map(item => item.title))
.then(title => {
	return title;
});

let printListItems = async () => {
	let titles = await listItems;
	console.log(titles);
}

printListItems();


// 2
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'GET',
	headers: {
		'Content-type': 'application/json'
	}
})
.then(response => response.json())
.then(json => {
	console.log(json);
	console.log(`The item ${json.title} on the list has a status of ${json.completed}`);
});


// 3
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "Created To Do List Item",
		"completed": false
	})
})
.then(response => response.json())
.then(json => console.log(json));


// 4
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "Updated To Do List Item",
		"dateCompleted": "Pending",
		"description": "To update the my to do list with a different data structure",
		"status": "Pending"
	})
})
.then(response => response.json())
.then(json => console.log(json));


// 5
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"dateCompleted": "07/09/21",
		"status": "Complete"
	})
})
.then(response => response.json())
.then(json => console.log(json));


// 6
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'DELETE'
});