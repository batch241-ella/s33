// console.log("Hello World");

// JavaScript Synchronous vs Asynchronous
// Asynchronous - means that we can proceed to execute other statements, while time consuming code is running in the background.

// FETCH METHOD
// Fetch() method returns a promise that resolves to a response object
// promise - is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// then() captures the response object and returns another promise which will eventually be resolved or rejected
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
// "json()" - from the response object to convert the data retrieved into JSON format to be used in our application
.then(response => response.json())
// print the converted JSON value from the fetch request
.then(json => console.log(json));
// "promise chain" - using multiple .then()


// async and await

async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}
fetchData();


// Creating a post
fetch("https://jsonplaceholder.typicode.com/posts", {
	// Sets the method of the request object
	method: 'POST',
	// Sets the header data of the "request object" to be sent to the backend
	headers: {
		'Content-type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userId": 1,
		"title": "Create Post",
		"body": "Create Post File"
	})
})
.then(response => response.json())
.then(json => console.log(json));


// Mini-activity
// Updating a post using PUT method
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "Update Post",
		"body": "Update Post File"
	})
})
.then(response => response.json())
.then(json => console.log(json));


// Deleting a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: 'DELETE'
});